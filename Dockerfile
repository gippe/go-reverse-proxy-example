FROM golang:1.13-alpine as builder

RUN apk update && apk add --no-cache git ca-certificates tzdata && update-ca-certificates && adduser -D -g '' appuser

WORKDIR /src
COPY ./ ./

ENV CGO_ENABLED=0
ENV GOOS=linux
ENV GOARCH=amd64
ENV GO111MODULE=on

RUN go build -a -installsuffix cgo -ldflags '-w -s' -o bin/go-reverse-proxy-example -v

FROM scratch AS final

COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /src/bin/* ./

USER appuser

ENTRYPOINT ["./go-reverse-proxy-example"]