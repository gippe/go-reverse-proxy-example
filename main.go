package main

import (
	"net/http"

	"gitlab.com/gippe/go-reverse-proxy-example/app"

	log "github.com/sirupsen/logrus"
)

var mux = http.NewServeMux()

func main() {
	server := &http.Server{
		Handler: mux,
		Addr:    ":8080",
	}

	mux.Handle("/items",
		http.HandlerFunc(app.HandleItems))

	log.Fatal(server.ListenAndServe())
}
