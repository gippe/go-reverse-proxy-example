package contexts

import (
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

func TestBuildItemsRequestContext(t *testing.T) {
	type args struct {
		req *http.Request
	}
	tests := []struct {
		name string
		args args
		want ItemsContext
	}{
		{
			"ExtractRequestedIds",
			args{httptest.NewRequest("GET", "http://example.com/items?ids=2", nil)},
			ItemsContext{
				PaginationDetails: PaginationDetails{
					Top:  defaultTopValue,
					Skip: 0,
				},
				RequestedIds: map[int]bool{2: true},
				MissingIds:   nil,
			},
		},
		{
			"ExtractPaginationDetails",
			args{httptest.NewRequest("GET", "http://example.com/items?top=1&skip=13", nil)},
			ItemsContext{
				PaginationDetails: PaginationDetails{
					Top:  1,
					Skip: 13,
				},
				RequestedIds: nil,
				MissingIds:   map[int]bool{14: true},
			},
		},
		{
			"ExtractMissingIdsNoSkip",
			args{httptest.NewRequest("GET", "http://example.com/items?missing=2&top=1", nil)},
			ItemsContext{
				PaginationDetails: PaginationDetails{
					Top:  1,
					Skip: 0,
				},
				RequestedIds: nil,
				MissingIds:   map[int]bool{2: true},
			},
		},
		{
			"ExtractMissingIdsWithSkip",
			args{httptest.NewRequest("GET", "http://example.com/items?missing=4&top=1&skip=5", nil)},
			ItemsContext{
				PaginationDetails: PaginationDetails{
					Top:  1,
					Skip: 5,
				},
				RequestedIds: nil,
				MissingIds:   map[int]bool{4: true},
			},
		},
		{
			"ExtractMissingIdsWithSkipAndTop",
			args{httptest.NewRequest("GET", "http://example.com/items?top=1&skip=5", nil)},
			ItemsContext{
				PaginationDetails: PaginationDetails{
					Top:  1,
					Skip: 5,
				},
				RequestedIds: nil,
				MissingIds:   map[int]bool{6: true},
			},
		},
		{
			"ExtractMissingIdsOnlyWithTop",
			args{httptest.NewRequest("GET", "http://example.com/items?top=1", nil)},
			ItemsContext{
				PaginationDetails: PaginationDetails{
					Top:  1,
					Skip: 0,
				},
				RequestedIds: nil,
				MissingIds:   map[int]bool{1: true},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := BuildItemsRequestContext(tt.args.req); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("BuildItemsRequestContext() = %v, want %v", got, tt.want)
			}
		})
	}
}
