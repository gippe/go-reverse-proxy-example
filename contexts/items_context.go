package contexts

import (
	"net/http"
	"strconv"
	"strings"
)

type key int

const (
	defaultTopValue        int = 50
	ItemsRequestContextKey key = iota
)

type ItemsContext struct {
	RequestedIds      map[int]bool
	MissingIds        map[int]bool
	PaginationDetails PaginationDetails
}

type PaginationDetails struct {
	Top  int
	Skip int
}

func ExtractRequestedIds(r *http.Request) map[int]bool {
	idsParam, ok := r.URL.Query()["ids"]
	requestedIds := make(map[int]bool)
	if ok && len(idsParam[0]) > 0 {
		ids := strings.Split(idsParam[0], ",")
		for _, value := range ids {
			id, err := strconv.Atoi(value)
			if err == nil {
				requestedIds[id] = true
			}
		}
	}
	return requestedIds
}

func ExtractMissingIds(r *http.Request, paginationDetails PaginationDetails) map[int]bool {
	idsParam, ok := r.URL.Query()["missing"]
	missingIds := make(map[int]bool)
	if ok && len(idsParam[0]) > 0 {
		ids := strings.Split(idsParam[0], ",")
		for _, value := range ids {
			id, err := strconv.Atoi(value)
			if err == nil {
				missingIds[id] = true
			}
		}
	}
	for i := paginationDetails.Skip + len(missingIds); i < paginationDetails.Skip+paginationDetails.Top; i++ {
		missingIds[i+1] = true
	}
	return missingIds
}

func ExtractPaginationDetails(r *http.Request) PaginationDetails {
	var err error
	var topValue, skipValue int
	topParam, ok := r.URL.Query()["top"]
	if !ok || len(topParam[0]) == 0 {
		topValue = defaultTopValue
	} else {
		topValue, err = strconv.Atoi(topParam[0])
		if err != nil {
			topValue = defaultTopValue
		}
	}
	skipParam, ok := r.URL.Query()["skip"]
	if !ok || len(skipParam[0]) == 0 {
		skipValue = 0
	} else {
		skipValue, err = strconv.Atoi(skipParam[0])
		if err != nil {
			skipValue = 0
		}
	}
	paginationDetails := PaginationDetails{
		Top:  topValue,
		Skip: skipValue,
	}
	return paginationDetails
}

func BuildItemsRequestContext(req *http.Request) ItemsContext {
	requestedIds := ExtractRequestedIds(req)
	paginationDetails := ExtractPaginationDetails(req)
	if len(requestedIds) > 0 {
		itemsRequestContext := ItemsContext{
			PaginationDetails: paginationDetails,
			RequestedIds:      requestedIds,
			MissingIds:        nil,
		}
		return itemsRequestContext
	} else {
		missingIds := ExtractMissingIds(req, paginationDetails)

		itemsRequestContext := ItemsContext{
			PaginationDetails: paginationDetails,
			RequestedIds:      nil,
			MissingIds:        missingIds,
		}
		return itemsRequestContext
	}
}
