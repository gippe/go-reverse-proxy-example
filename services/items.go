package services

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gippe/go-reverse-proxy-example/contexts"
	"gitlab.com/gippe/go-reverse-proxy-example/models"
)

const ItemsServiceTimeout = time.Duration(900 * time.Millisecond)

type ItemsServiceError struct {
	error   error
	timeout bool
}

type ItemsServiceEndpoint struct {
	URL string
}

type ItemResponse struct {
	statusCode int
	idNotFound int
	item       models.Item
}

func FetchItems(endpoint string, itemsRequestContext contexts.ItemsContext, ctx context.Context) (models.Items, []int) {
	if len(itemsRequestContext.RequestedIds) > 0 {
		items, idsNotFound := fetchItemsForIds(endpoint, itemsRequestContext.RequestedIds, ctx)
		models.SortItemsByAge(items)
		return items, idsNotFound
	} else {
		items, idsNotFound := fetchItemsForIds(endpoint, itemsRequestContext.MissingIds, ctx)
		return items, idsNotFound
	}
}

func fetchItemsForIds(endpoint string, idsToFetch map[int]bool, ctx context.Context) (models.Items, []int) {
	client := &http.Client{
		Timeout: 1 * time.Second,
	}

	var errCh = make(chan ItemsServiceError, 1)
	var responsesCh = make(chan ItemResponse)

	var items = make([]models.Item, 0, len(idsToFetch))
	var idsNotFound = make([]int, 0, len(idsToFetch))
	for id := range idsToFetch {
		go fetchItem(endpoint, id, errCh, responsesCh, ctx, client)
	}
	for range idsToFetch {
		select {
		case itemsServiceError := <-errCh:
			err := itemsServiceError.error
			if err != nil {
				if itemsServiceError.timeout {
					log.WithField("msg", "Timeout").WithField("service", "items").Warn()
				}
				return items, idsNotFound
			}
		case response := <-responsesCh:
			if response.statusCode == http.StatusOK {
				items = append(items, response.item)
			} else if response.statusCode == http.StatusNotFound {
				idsNotFound = append(idsNotFound, response.idNotFound)
			}
		}
	}
	return items, idsNotFound
}

func fetchItem(endpoint string, id int, errCh chan<- ItemsServiceError, respCh chan<- ItemResponse, ctx context.Context, client *http.Client) {
	url := fmt.Sprintf("%s/%d", endpoint, id)
	req, _ := http.NewRequest(http.MethodGet, url, nil)
	req = req.WithContext(ctx)
	resp, err := client.Do(req)
	if err != nil {
		select {
		case <-ctx.Done():
			errCh <- ItemsServiceError{
				error:   ctx.Err(),
				timeout: true,
			}
			return
		default:
			log.WithField("msg", err.Error()).WithField("service", "items").Error()
			errCh <- ItemsServiceError{
				error:   ctx.Err(),
				timeout: false,
			}
			return
		}
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusNotFound {
		respCh <- ItemResponse{resp.StatusCode, id, models.Item{}}
	} else if resp.StatusCode == http.StatusOK {
		item := models.Item{}
		if err := json.NewDecoder(resp.Body).Decode(&item); err != nil {
			log.WithField("msg", err.Error()).WithField("service", "items").Error()
			errCh <- ItemsServiceError{
				error:   err,
				timeout: false,
			}
			return
		}
		respCh <- ItemResponse{resp.StatusCode, 0, item}
	}
	return
}
