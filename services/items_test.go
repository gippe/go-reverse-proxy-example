package services

import (
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
	"time"

	"gitlab.com/gippe/go-reverse-proxy-example/contexts"
	"gitlab.com/gippe/go-reverse-proxy-example/models"
)

func buildContextWithHighTimeout(requestContext contexts.ItemsContext) context.Context {
	ctx, _ := context.WithTimeout(context.Background(), time.Duration(2*ItemsServiceTimeout))

	ctx = context.WithValue(ctx, contexts.ItemsRequestContextKey, requestContext)
	return ctx
}

func buildContextWithLowTimeout(requestContext contexts.ItemsContext) context.Context {
	ctx, _ := context.WithTimeout(context.Background(), time.Duration(1*time.Microsecond))

	ctx = context.WithValue(ctx, contexts.ItemsRequestContextKey, requestContext)
	return ctx
}

var contextForFetchingItemsByMissingIdsWithTimeout = contexts.ItemsContext{
	PaginationDetails: contexts.PaginationDetails{
		Top:  1,
		Skip: 0,
	},
	RequestedIds: nil,
	MissingIds:   map[int]bool{8: true},
}

var contextForFetchingItemsByMissingIds = contexts.ItemsContext{
	PaginationDetails: contexts.PaginationDetails{
		Top:  2,
		Skip: 0,
	},
	RequestedIds: nil,
	MissingIds:   map[int]bool{4: true, -1: true},
}

var contextForFetchingItemsByRequestedIds = contexts.ItemsContext{
	PaginationDetails: contexts.PaginationDetails{
		Top:  2,
		Skip: 0,
	},
	RequestedIds: map[int]bool{4: true, 8: true},
	MissingIds:   nil,
}

var contextForFetchingItemsByRequestedIdsWithTimeout = contexts.ItemsContext{
	PaginationDetails: contexts.PaginationDetails{
		Top:  2,
		Skip: 0,
	},
	RequestedIds: map[int]bool{4: true, 16: true},
	MissingIds:   nil,
}

func TestFetchItems(t *testing.T) {
	type args struct {
		itemsRequestContext contexts.ItemsContext
		ctx                 context.Context
	}
	tests := []struct {
		name  string
		args  args
		want  models.Items
		want1 []int
	}{
		{
			"FetchItemsByMissingIds",
			args{
				itemsRequestContext: contextForFetchingItemsByMissingIds,
				ctx:                 buildContextWithHighTimeout(contextForFetchingItemsByMissingIds),
			},
			models.Items{item4},
			// We do not have validation on item.Ids here. Check Pagination.
			[]int{-1},
		},
		{
			"FetchItemsByMissingIdsWithTimeout",
			args{
				itemsRequestContext: contextForFetchingItemsByMissingIdsWithTimeout,
				ctx:                 buildContextWithLowTimeout(contextForFetchingItemsByMissingIdsWithTimeout),
			},
			models.Items{},
			[]int{},
		},
		{
			"FetchItemsByRequestedIds",
			args{
				itemsRequestContext: contextForFetchingItemsByRequestedIds,
				ctx:                 buildContextWithHighTimeout(contextForFetchingItemsByRequestedIds),
			},
			models.Items{item8, item4},
			[]int{},
		},
		{
			"FetchItemsByRequestedIdsWithTimeout",
			args{
				itemsRequestContext: contextForFetchingItemsByRequestedIdsWithTimeout,
				ctx:                 buildContextWithHighTimeout(contextForFetchingItemsByRequestedIdsWithTimeout),
			},
			models.Items{item4},
			[]int{},
		},
	}
	for _, tt := range tests {
		var apiStub = mockItemsAPI()
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := FetchItems(apiStub.URL, tt.args.itemsRequestContext, tt.args.ctx)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FetchItems() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("FetchItems() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

// Mocks and Fixtures
func mockItemsAPI() *httptest.Server {
	handler := func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		switch r.RequestURI {
		case "/16": // Return item8 with a significant delay
			w.WriteHeader(http.StatusOK)
			enc := json.NewEncoder(w)
			enc.SetEscapeHTML(false)
			json.NewEncoder(w).Encode(item8)
			out, _ := json.Marshal(item8)
			// Simulate slow response from API
			time.Sleep(time.Duration(2 * ItemsServiceTimeout))
			w.Write(out)
		case "/8":
			w.WriteHeader(http.StatusOK)
			enc := json.NewEncoder(w)
			enc.SetEscapeHTML(false)
			json.NewEncoder(w).Encode(item8)
			out, _ := json.Marshal(item8)
			w.Write(out)
		case "/4":
			w.WriteHeader(http.StatusOK)
			enc := json.NewEncoder(w)
			enc.SetEscapeHTML(false)
			json.NewEncoder(w).Encode(item4)
			out, _ := json.Marshal(item4)
			w.Write(out)
		default:
			http.Error(w, "not found", http.StatusNotFound)
			return
		}
	}
	ts := httptest.NewServer(http.HandlerFunc(handler))

	return ts
}

var item8 = models.Item{
	Id:   "8",
	Name: "Item8",
	Age:  "30",
}

var item4 = models.Item{
	Id:  "4",
	Name: "Item4",
	Age: "45",
}
