## Problem Description:

There is an external API endpoint which allows to query items by ID: `https://<ENDPOINT>/<ID>`

Design an application which returns either:
* An aggregation of all the items returned by the endpoint when the query parameter `ids` is not set.
* All the items with IDs matching the ones passed by the query parameter `ids`

The query to the application should look like: `https://example.com/items?ids=1,3,4`

Implement sorting of the response by the field `age`.
Implement pagination.

## Run the application (with Docker):

The only requirement is to have a working Docker installation.

To run the application which will be listening on port 8080:
> make docker-run

## Run Tests (and/or the application without Docker):

To run the application (without docker) and to run the test suite, these are the requirements:
* Go 1.13

Run the test suite:
> make test

Run test suite and application:
> make

Please refer to Makefile for other useful shortcuts.

## Documentation:

This project consists on a single HTTP handler serving data (in JSON format) for two use cases: index of all items and item by id.

Thanks to the use of a context with timeout, we can set a maximum response time requirement.

In order to improve the application efficiency, the service is handling each HTTP request concurrently by delegating them to go routines.

When any of the external HTTP requests handled by those go routines times out, the signal is shared with the main application workflow via the error channel, allowing for a prompt return of results from the service.

Thanks to the context package changes introduced in Golang version 1.7, it was easy to store and share data from the request with the rest of the application.

Hence, by storing key/value mappings in the context, it was straightforward to keep track of the information needed for pagination.
This way we avoided storing state on the application side and used instead the next call URL for this purpose.

This URL contains a cursor (skip parameter) and all the item ids which were not fetched in the previous call (missing parameter).
This way we were able to ensure that each item was not requested again if it was already returned in a previous call.

The use of third party libraries was limited to these two packages:

* logrus: improve the application logging thanks by providing structured logging
* testify: to make testing easier by providing common assertions and mocks

Both libraries are completely optional but were very helpful and they also play nicely with the standard libraries.

## Todo:

* Handle errors where they are not handled
