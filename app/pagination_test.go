package app

import (
	"math"
	"reflect"
	"testing"

	"gitlab.com/gippe/go-reverse-proxy-example/models"
)

func Test_extractFirstItemNotFound(t *testing.T) {
	type args struct {
		idsNotFound []int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "WhenThereAreNoIdsNotFound",
			args: args{
				idsNotFound: []int{},
			},
			want: math.MaxInt32,
		},
		{
			name: "WhenThereAreIdsNotFound",
			args: args{
				idsNotFound: []int{123, -23, 41, 6, 78, 9},
			},
			want: 6,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := extractFirstItemNotFound(tt.args.idsNotFound); got != tt.want {
				t.Errorf("extractFirstItemNotFound() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_buildMissingIdsForNextCall(t *testing.T) {
	type args struct {
		missingIds          map[int]bool
		items               models.Items
		firstItemIdNotFound int
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "OnlyMissingIds",
			args: args{
				missingIds:          map[int]bool{1: true},
				items:               models.Items{},
				firstItemIdNotFound: math.MaxInt32,
			},
			want: []string{"1"},
		},
		{
			name: "WithMissingIdsAndFetchedItems",
			args: args{
				missingIds:          map[int]bool{8: true, 2: true},
				items:               models.Items{{Id: "8"}},
				firstItemIdNotFound: math.MaxInt32,
			},
			want: []string{"2"},
		},
		{
			name: "WithMissingIdsFetchedItemsAndFirstItemIdNotFound",
			args: args{
				missingIds:          map[int]bool{8: true, 2: true, 3: true, 4: true},
				items:               models.Items{{Id: "3"}},
				firstItemIdNotFound: 3,
			},
			want: []string{"2"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := buildMissingIdsForNextCall(tt.args.missingIds, tt.args.items, tt.args.firstItemIdNotFound); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("buildMissingIdsForNextCall() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_buildNextCallStr(t *testing.T) {
	type args struct {
		skip                  int
		firstItemIdNotFound   int
		missingIdsForNextCall []string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "WithMissingIdsForNextCall",
			args: args{
				skip:                  6,
				missingIdsForNextCall: []string{"1", "2"},
				firstItemIdNotFound:   5,
			},
			want: "http://localhost:8080/items?missing=1%2C2",
		},
		{
			name: "WithSkip",
			args: args{
				skip:                  4,
				missingIdsForNextCall: []string{},
				firstItemIdNotFound:   5,
			},
			want: "http://localhost:8080/items?skip=4",
		},
		{
			name: "WithMissingIdsForNextCallAndSkip",
			args: args{
				skip:                  4,
				missingIdsForNextCall: []string{"1", "2"},
				firstItemIdNotFound:   5,
			},
			want: "http://localhost:8080/items?missing=1%2C2&skip=4",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := buildNextCallStr(tt.args.skip, tt.args.firstItemIdNotFound, tt.args.missingIdsForNextCall); got != tt.want {
				t.Errorf("buildNextCallStr() = %v, want %v", got, tt.want)
			}
		})
	}
}
