package app

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/gippe/go-reverse-proxy-example/models"
)

var testResponse = httptest.NewRecorder()

func Test_writeResponse(t *testing.T) {
	type args struct {
		w        http.ResponseWriter
		response interface{}
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "SetsContentTypeAndStatusCode",
			args: args{
				testResponse,
				AllItemsResponse{
					"",
					models.Items{},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			writeResponse(tt.args.w, tt.args.response)
			assert.Equal(t, testResponse.Code, http.StatusOK)
			assert.Equal(t, testResponse.Header().Get("content-type"), "application/json; charset=utf-8")
		})
	}
}

func TestHandleItems(t *testing.T) {
	type args struct {
		w   http.ResponseWriter
		req *http.Request
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			HandleItems(tt.args.w, tt.args.req)
		})
	}
}
