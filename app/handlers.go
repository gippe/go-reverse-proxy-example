package app

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gippe/go-reverse-proxy-example/contexts"
	"gitlab.com/gippe/go-reverse-proxy-example/services"
)

const (
	itemServiceEndpoint string = "<ADD_ENDPOINT_HERE>"
)

func HandleItems(w http.ResponseWriter, req *http.Request) {
	start := time.Now()
	var (
		ctx    context.Context
		cancel context.CancelFunc
	)

	ctx, cancel = context.WithTimeout(context.Background(), services.ItemsServiceTimeout)
	defer cancel() // Cancel ctx as soon as HandleItems returns

	itemsRequestContext := contexts.BuildItemsRequestContext(req)
	ctx = context.WithValue(ctx, contexts.ItemsRequestContextKey, itemsRequestContext)

	items, idsNotFound := services.FetchItems(itemServiceEndpoint, itemsRequestContext, ctx)

	response := BuildResponse(items, idsNotFound, itemsRequestContext)
	writeResponse(w, response)
	elapsed := time.Since(start)
	log.WithField("duration", elapsed)
}

func writeResponse(w http.ResponseWriter, response interface{}) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	enc := json.NewEncoder(w)
	enc.SetEscapeHTML(false)
	json.NewEncoder(w).Encode(response)
}
