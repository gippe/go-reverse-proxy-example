package app

import (
	"gitlab.com/gippe/go-reverse-proxy-example/contexts"
	"gitlab.com/gippe/go-reverse-proxy-example/models"
)

type ItemsResponse struct {
	Items models.Items `json:"items"`
}

type AllItemsResponse struct {
	NextCall string       `json:"nextCall"`
	Items    models.Items `json:"items"`
}

func BuildResponse(items models.Items, idsNotFound []int, requestContext contexts.ItemsContext) interface{} {
	if len(requestContext.RequestedIds) > 0 {
		return ItemsResponse{
			items,
		}
	} else {
		nextCall := GenerateNextCall(requestContext, items, idsNotFound)
		return AllItemsResponse{
			NextCall: nextCall,
			Items:    items,
		}
	}
}
