package app

import (
	"math"
	"net/url"
	"sort"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gippe/go-reverse-proxy-example/contexts"
	"gitlab.com/gippe/go-reverse-proxy-example/models"
)

func GenerateNextCall(itemsRequestContext contexts.ItemsContext, items models.Items, idsNotFound []int) string {
	firstItemIdNotFound := extractFirstItemNotFound(idsNotFound)
	paginationDetails := itemsRequestContext.PaginationDetails
	missingIds := itemsRequestContext.MissingIds
	missingIdsForNextCall := buildMissingIdsForNextCall(missingIds, items, firstItemIdNotFound)
	skip := paginationDetails.Skip + len(items)
	outputStr := buildNextCallStr(skip, firstItemIdNotFound, missingIdsForNextCall)

	return outputStr
}

func extractFirstItemNotFound(idsNotFound []int) int {
	firstItemIdNotFound := math.MaxInt32
	if len(idsNotFound) > 0 {
		sort.Ints(idsNotFound)
		for _, id := range idsNotFound {
			if id > 0 {
				firstItemIdNotFound = id
				break
			}
		}
	}
	return firstItemIdNotFound
}

func buildMissingIdsForNextCall(missingIds map[int]bool, items models.Items, firstItemIdNotFound int) []string {
	missingMap := make(map[int]bool)
	for id := range missingIds {
		missingMap[id] = true
	}
	for _, item := range items {
		id, err := strconv.Atoi(item.Id)
		if err == nil {
			missingMap[id] = false
		}
	}
	var missingIdsForNextCall = make([]string, 0)
	for id, isMissing := range missingMap {
		if isMissing && id < firstItemIdNotFound {
			missingIdsForNextCall = append(missingIdsForNextCall, strconv.Itoa(id))
		}
	}
	return missingIdsForNextCall
}

func buildNextCallStr(skip int, firstItemIdNotFound int, missingIdsForNextCall []string) string {
	u, err := url.Parse("http://localhost:8080/items")
	if err != nil {
		log.Fatal(err)

	}
	q := u.Query()
	if len(missingIdsForNextCall) > 0 {
		q.Set("missing", strings.Join(missingIdsForNextCall, ","))
	}
	if skip <= firstItemIdNotFound-1 {
		q.Set("skip", strconv.Itoa(skip))
	}
	u.RawQuery = q.Encode()
	return u.String()
}
