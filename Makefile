# Go
BINARY_NAME=go-reverse-proxy-example
BINARY_UNIX=$(BINARY_NAME)_unix

# Docker
GO_BASE_IMAGE=golang:1.13-alpine
PROJECT_PATH=/go/src/gitlab.com/gippe/go-reverse-proxy-example
DOCKER_IMAGE=gippe/go-reverse-proxy-example

# Local
all: clean test run

build: clean
	go build -o bin/$(BINARY_NAME) -v

test:
	go test -v ./...

clean:
	go clean
	rm -rf vendor/*
	rm -f bin/$(BINARY_NAME)
	rm -f bin/$(BINARY_UNIX)

run: build
	./bin/$(BINARY_NAME)

# Cross compilation
build-linux:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o $(BINARY_UNIX) -v

docker-build:
	docker run --rm -v "$$PWD":$(PROJECT_PATH) -w $(PROJECT_PATH) $(GO_BASE_IMAGE) go build -o bin/$(BINARY_NAME)

docker-build-image: docker-build
	docker build -t $(DOCKER_IMAGE) .

docker-run: docker-build-image
	docker run --rm -p 8080:8080 $(DOCKER_IMAGE)
