module gitlab.com/gippe/go-reverse-proxy-example

go 1.13

require (
	github.com/sirupsen/logrus v1.2.0
	github.com/stretchr/testify v1.2.2
	golang.org/x/crypto v0.0.0-20181106171534-e4dc69e5b2fd // indirect
	golang.org/x/sys v0.0.0-20181107165924-66b7b1311ac8 // indirect
)
