package models

import (
	"reflect"
	"testing"
)

func TestSortItemsByAge(t *testing.T) {
	type args struct {
		items []Item
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "SortedByAscendingAge",
			args: args{
				items: []Item{{Id: "1", Age: "10"}, {Id: "2", Age: "2"}, {Id: "3", Age: ""}},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			SortItemsByAge(tt.args.items)
			got := tt.args.items
			want := []Item{{Id: "2", Age: "2"}, {Id: "1", Age: "10"}, {Id: "3", Age: ""}}
			if !reflect.DeepEqual(got, want) {
				t.Errorf("FetchItems() got = %v, want %v", got, want)
			}
		})
	}
}
