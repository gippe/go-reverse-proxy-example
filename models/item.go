package models

import (
	"math"
	"regexp"
	"sort"
	"strconv"
)

type Item struct {
	Id          string `json:"id"`
	Name        string `json:"name"`
	Age         string `json:"age"`
}

type Items []Item

func SortItemsByAge(items []Item) {
	sort.Slice((items)[:], func(i, j int) bool {
		re := regexp.MustCompile("(\\d*)")
		ageIStr := re.FindStringSubmatch(items[i].Age)
		ageJStr := re.FindStringSubmatch(items[j].Age)
		var ageI, ageJ int
		var err error
		if len(ageIStr) > 1 {
			ageI, err = strconv.Atoi(ageIStr[1])
			if err != nil {
				ageI = math.MaxInt32
			}
		} else {
			ageI = math.MaxInt32
		}
		if len(ageJStr) > 1 {
			ageJ, err = strconv.Atoi(ageJStr[1])
			if err != nil {
				ageJ = math.MaxInt32
			}
		} else {
			ageJ = math.MaxInt32
		}
		return ageI < ageJ
	})
}
